package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class SimpleAppS15 {
	private JTextArea jta;

	public static void main(String[] args) {
		new SimpleAppS15();
	}

	public SimpleAppS15() {
		JFrame frame = new JFrame("A simple application");
		frame.setLayout(new BorderLayout());

		jta = new JTextArea(20, 60);
		jta.setEditable(false);
		JScrollPane jsp = new JScrollPane(jta);
		frame.add(jsp, BorderLayout.CENTER);
		JButton button = new JButton("Press me");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				jta.append("Button was clicked\n");
			}
		});

		frame.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent arg0) {
				jta.append("Mouse is at " + arg0.getX() + "," + arg0.getY()
						+ "\n");
			}

			@Override
			public void mouseDragged(MouseEvent arg0) {
			}
		});
		Box box = Box.createHorizontalBox();
		box.setBorder(new LineBorder(Color.red));
		box.add(button);
		frame.add(box, BorderLayout.SOUTH);

		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setVisible(true);
	}
}
