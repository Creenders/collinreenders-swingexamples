package swingExample;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class MouseLabel extends JLabel 
           	implements MouseListener, ActionListener {

     public int clickCount;
//-----------------------------------------------------------------
// Constructor

      MouseLabel() {
          super("Nothing yet");
          clickCount=0;
          this.addMouseListener(this);
      }
   
//-----------------------------------------------------------------
// Methods from MouseListener interface

      public void mouseClicked(MouseEvent e) {
          setText("mouse click");
      }
     
      public void mouseEntered(MouseEvent e) {
          setText("mouse enter");
      }
     
      public void mouseExited(MouseEvent e) {
          setText("mouse exit");
      }
     
      public void mousePressed(MouseEvent e) {
          setText("mouse press");
      }
     
      public void mouseReleased(MouseEvent e) {
          setText("mouse release");
      }
     
//-----------------------------------------------------------------
// Method from ActionListener interface

      public void actionPerformed(ActionEvent e) {
               clickCount++;
               setText("Clicked: "+clickCount);
      }
//-----------------------------------------------------------------

}
